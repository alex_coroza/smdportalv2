<?php defined('SYSPATH') OR die('No direct access allowed.');

class Etc_Utility {


	// generates a random string(all caps) with length based on $length
	public function generateRandomString($length = 10) {
		$characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$randomString ="";
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}

		return $randomString;
	}



	public function computeAge($birthDate) {
		$date = new DateTime($birthDate);
		$now = new DateTime();
		$interval = $now->diff($date);
		return $interval->y;
	}




	public function login($userName, $password) {
		$account = ORM::factory('orm_account', $userName);
		$result = new stdClass();
		
		if($account->password == sha1(md5(sha1($password.$account->salt)))) {
			session_start();
			$_SESSION['user'] = $account->userName;
			$result->message = 'success';
			$result->type = $account->type;
		} else {
			$result->message = 'failed';
			$result->type = '';
		}

		return $result;
	}


	
	public function logout() {
		session_start();
		session_destroy();
	}




	public function redirectUnauthorizedUser($type) {
		session_start();
		$accountModel = new Model_Account();

		if(!isset($_SESSION['user']) && $type != 'guest') {
			header('Location: /worktrustedV2/');
			die();
		}

		if(isset($_SESSION['user'])) {
			$readOptions = (object)array(
				'userName' => $_SESSION['user']
			);
			$account = $accountModel->read($readOptions);
			if($account->type != $type) {
				header('Location: /worktrustedV2/'.$account->type.'/');
				die();
			}
		}
	}





}